-- name: all_fks?
-- retrieves all foreign key relationships.
-- param: schema: &str - default search schema.
select
  coalesce(nullif(originnsp.nspname, :schema) || '.', '') || originrel.relname,
  coalesce(nullif(dependentnsp.nspname, :schema) || '.', '') || dependentrel.relname
from pg_catalog.pg_constraint as foreign_keys
join pg_catalog.pg_class as originrel on originrel.oid = foreign_keys.confrelid
left outer join pg_catalog.pg_namespace as originnsp on originnsp.oid = originrel.relnamespace
join pg_catalog.pg_class as dependentrel on dependentrel.oid = foreign_keys.conrelid
left outer join pg_catalog.pg_namespace as dependentnsp on dependentnsp.oid = dependentrel.relnamespace
where originrel.relispartition is false
  and dependentrel.relispartition is false
  and foreign_keys.contype = 'f';

-- name: all_fks_postgraphile?
-- retrieves Postgraphile foreignKey relationships.
-- param: schema: &str - default search schema.
select
  coalesce(nullif(originnsp.nspname, :schema) || '.', '') || originrel.relname,
  coalesce(nullif(dependentnsp.nspname, :schema) || '.', '') || dependentrel.relname
from pg_class as dependentrel
join pg_namespace as dependentnsp on dependentnsp.oid = dependentrel.relnamespace
join pg_description d on d.objoid = dependentrel.oid
join lateral regexp_split_to_table(d.description, '\n') as u (line) on true
left join pg_class as originrel
  on trim(substring(u.line, '\sreferences\s+([^\(]+)\('))::regclass = originrel.oid
join pg_namespace as originnsp on originnsp.oid = originrel.relnamespace;

-- name: all_inheritors?
-- retrieves all inheritance relationships.
-- param: schema: &str - default search schema.
select
  coalesce(nullif(parentns.nspname::text, :schema) || '.', '') || parentrel.relname::text as parent,
  coalesce(nullif(childns.nspname::text, :schema) || '.', '') || childrel.relname::text as child
from pg_catalog.pg_inherits as pgi
join pg_catalog.pg_class as parentrel on parentrel.oid = pgi.inhparent
join pg_catalog.pg_namespace as parentns on parentns.oid = parentrel.relnamespace
join pg_catalog.pg_class as childrel on childrel.oid = pgi.inhrelid
join pg_catalog.pg_namespace as childns on childns.oid = childrel.relnamespace;

-- name: fks?
-- retrieves foreign key relationships starting at a single table.
-- param: schema: &str - starting schema.
-- param: table: &str - starting table.
with recursive all_fks as (
  select
    originrel.relname::text as originrel,
    originns.nspname::text as originns,
    dependentrel.relname::text as dependentrel,
    dependentns.nspname::text as dependentns,
    'constraint' as kind
  from pg_catalog.pg_constraint as foreign_keys
  join pg_catalog.pg_class as originrel on originrel.oid = foreign_keys.confrelid
  join pg_catalog.pg_namespace as originns on originns.oid = originrel.relnamespace
  join pg_catalog.pg_class as dependentrel on dependentrel.oid = foreign_keys.conrelid
  join pg_catalog.pg_namespace as dependentns on dependentns.oid = dependentrel.relnamespace
  where dependentrel.relispartition is false and foreign_keys.contype = 'f'
), keys_dn as (
  select
    1 as level,
    :table::text collate "C" as origin,
    :schema::text collate "C" as origin_schema,
    :table::text collate "C" as dependent,
    :schema::text collate "C" as dependent_schema,
    null as kind
  union
  select 2, all_fks.originrel, all_fks.originns, all_fks.dependentrel, all_fks.dependentns, all_fks.kind
  from keys_dn
  join all_fks on all_fks.dependentns = keys_dn.origin_schema and all_fks.dependentrel = keys_dn.origin
), keys_up as (
  select
    1 as level,
    :table::text collate "C" as origin,
    :schema::text collate "C" as origin_schema,
    :table::text collate "C" as dependent,
    :schema::text collate "C" as dependent_schema,
    null as kind
  union
  select 2, all_fks.originrel, all_fks.originns, all_fks.dependentrel, all_fks.dependentns, all_fks.kind
  from keys_up
  join all_fks on all_fks.originns = keys_up.dependent_schema and all_fks.originrel = keys_up.dependent
), filtered_keys as (
  select origin, nullif(origin_schema, :schema) as origin_schema, dependent, nullif(dependent_schema, :schema) as dependent_schema
  from keys_dn where level > 1
  union
  select origin, nullif(origin_schema, :schema), dependent, nullif(dependent_schema, :schema)
  from keys_up where level > 1
)
select
  coalesce(origin_schema || '.', '') || origin,
  coalesce(dependent_schema || '.', '') || dependent,
  'constraint' as kind
from filtered_keys;

-- name: fks_postgraphile?
-- retrieves foreign key relationships starting at a single table, including
-- Postgraphile @foreignKeys.
-- param: schema: &str - starting schema.
-- param: table: &str - starting table.
with recursive all_fks as (
  select
    originrel.relname::text as originrel,
    originns.nspname::text as originns,
    dependentrel.relname::text as dependentrel,
    dependentns.nspname::text as dependentns,
    'constraint' as kind
  from pg_catalog.pg_constraint as foreign_keys
  join pg_catalog.pg_class as originrel on originrel.oid = foreign_keys.confrelid
  join pg_catalog.pg_namespace as originns on originns.oid = originrel.relnamespace
  join pg_catalog.pg_class as dependentrel on dependentrel.oid = foreign_keys.conrelid
  join pg_catalog.pg_namespace as dependentns on dependentns.oid = dependentrel.relnamespace
  where dependentrel.relispartition is false and foreign_keys.contype = 'f'
  union
  select
    originrel.relname::text as originrel,
    originns.nspname::text as originns,
    dependentrel.relname::text as dependentrel,
    dependentns.nspname::text as dependentns,
    'postgraphile' as kind
  from pg_class as dependentrel
  join pg_namespace as dependentns on dependentns.oid = dependentrel.relnamespace
  join pg_description d on d.objoid = dependentrel.oid
  join lateral regexp_split_to_table(d.description, '\n') as u (line) on true
  left join pg_class as originrel
      on trim(substring(u.line, '\sreferences\s+([^\(]+)\('))::regclass = originrel.oid
  join pg_namespace as originns on originns.oid = originrel.relnamespace
), keys_dn as (
  select
    1 as level,
    :table::text collate "C" as origin,
    :schema::text collate "C" as origin_schema,
    :table::text collate "C" as dependent,
    :schema::text collate "C" as dependent_schema,
    null as kind
  union
  select 2, all_fks.originrel, all_fks.originns, all_fks.dependentrel, all_fks.dependentns, all_fks.kind
  from keys_dn
  join all_fks on all_fks.dependentns = keys_dn.origin_schema and all_fks.dependentrel = keys_dn.origin
), keys_up as (
  select
    1 as level,
    :table::text collate "C" as origin,
    :schema::text collate "C" as origin_schema,
    :table::text collate "C" as dependent,
    :schema::text collate "C" as dependent_schema,
    null as kind
  union
  select 2, all_fks.originrel, all_fks.originns, all_fks.dependentrel, all_fks.dependentns, all_fks.kind
  from keys_up
  join all_fks on all_fks.originns = keys_up.dependent_schema and all_fks.originrel = keys_up.dependent
), filtered_keys as (
  select
    origin,
    nullif(origin_schema, :schema) as origin_schema,
    dependent,
    nullif(dependent_schema, :schema) as dependent_schema,
    kind
  from keys_dn where level > 1
  union
  select origin, nullif(origin_schema, :schema), dependent, nullif(dependent_schema, :schema), kind
  from keys_up where level > 1
)
select
  coalesce(origin_schema || '.', '') || origin,
  coalesce(dependent_schema || '.', '') || dependent,
  kind
from filtered_keys;

-- name: views?
-- retrieves view inheritance relationships.
-- param: schema: &str - starting schema.
-- param: view: &str - starting view.
with recursive dependencies as (
  select distinct
    null collate "C" as parent_schema,
    pc.relname collate "C" as parent_name,
    pc.oid::oid as parent_oid,
    nullif(n.nspname, :schema) collate "C" as dependent_schema,
    c.relname::text collate "C" as dependent_name,
    c.oid as dependent_oid
  from pg_class as pc
  join pg_namespace as pn on pn.oid = pc.relnamespace
  join pg_depend as d on d.refobjid = pc.oid
  join pg_rewrite as r on r.oid = d.objid
  join pg_class as c on c.oid = r.ev_class
  join pg_namespace as n on n.oid = c.relnamespace
  where pn.nspname = :schema
    and pc.relname = :view
  union
  select distinct
    nullif(dependencies.dependent_schema, :schema),
    dependencies.dependent_name,
    dependencies.dependent_oid,
    nullif(n.nspname, :schema),
    c.relname,
    c.oid
  from dependencies
  join pg_depend as d on d.refobjid = dependencies.dependent_oid
  join pg_rewrite as r on r.oid = d.objid
  join pg_class as c on c.oid = r.ev_class
  join pg_namespace as n on n.oid = c.relnamespace
  where n.nspname not in ('information_schema', 'pg_catalog')
    and d.classid = 'pg_rewrite'::regclass
    and d.refclassid = 'pg_class'::regclass
    and c.relkind = 'v'
    and dependencies.dependent_oid <> c.oid
)
select
  coalesce(parent_schema || '.', '') || parent_name,
  coalesce(dependent_schema || '.', '') || dependent_name
from dependencies;

-- name: view_attrs?
-- retrieves view inheritance relationships filtered by a member column.
-- param: schema: &str - starting schema.
-- param: view: &str - starting view.
-- param: attr: &str - column to trace.
with recursive dependencies as (
  select distinct
    null collate "C" as parent_schema,
    pc.relname collate "C" as parent_name,
    pc.oid::oid as parent_oid,
    nullif(n.nspname, :schema) collate "C" as dependent_schema,
    c.relname::text collate "C" as dependent_name,
    c.oid as dependent_oid,
    pa.attnum,
    pa.attname
  from pg_class as pc
  join pg_namespace as pn on pn.oid = pc.relnamespace
  join pg_attribute as pa on pa.attrelid = pc.oid
  join pg_depend as d on d.refobjid = pc.oid and d.refobjsubid = pa.attnum
  join pg_rewrite as r on r.oid = d.objid
  join pg_class as c on c.oid = r.ev_class
  join pg_namespace as n on n.oid = c.relnamespace
  where pn.nspname = :schema
    and pc.relname = :view
    and pa.attname = :attr
  union
  select distinct
    nullif(dependencies.dependent_schema, :schema),
    dependencies.dependent_name,
    dependencies.dependent_oid,
    nullif(n.nspname, :schema),
    c.relname,
    c.oid,
    pa.attnum,
    pa.attname
  from dependencies
  join pg_depend as d on d.refobjid = dependencies.dependent_oid
  join pg_rewrite as r on r.oid = d.objid
  join pg_class as c on c.oid = r.ev_class
  join pg_namespace as n on n.oid = c.relnamespace
  join pg_attribute as pa on pa.attrelid = c.oid
  where n.nspname not in ('information_schema', 'pg_catalog')
    and d.classid = 'pg_rewrite'::regclass
    and d.refclassid = 'pg_class'::regclass
    and c.relkind = 'v'
    and dependencies.dependent_oid <> c.oid
    and pa.attnum = dependencies.attnum
)
select
  coalesce(parent_schema || '.', '') || parent_name,
  coalesce(dependent_schema || '.', '') || dependent_name
from dependencies

-- name: function_src->
-- retrieve database function source code.
-- param: name: &str - function name (including schema).
select pg_get_functiondef(:name::text::regproc);

-- name: triggers^
-- retrieve triggers on a table.
-- param: name: &str - table name (including schema).
-- param: op: &str - filter by operation.
with info as (
select
    t.tgname,
    (regexp_match(pg_get_triggerdef(t.oid), '.{35,} when \((.+)\) execute function'::text, 'i'))[1] as action_condition,
    case t.tgtype::integer & 1
      when 1 then 'row'::text
      else 'statement'::text
    end::information_schema.character_data as action_orientation,
    case t.tgtype::integer & 66
      when 2 then 'before'::text
      when 64 then 'instead of'::text
      else 'after'::text
    end::information_schema.character_data as action_timing,
    array_agg(am.action) as action_statement
  from pg_trigger as t
  join pg_proc as p on p.oid = t.tgfoid
  join (
      values (4, 'insert'), (8, 'delete'), (16, 'update')
  ) as am (bits, action) on t.tgtype & am.bits <> 0
  group by t.oid, t.tgtype
)
select
  p.pronamespace::regnamespace::text as schema_name,
  p.proname as function_name,
  format(
    '%1$s:\n%2$s %3$s for each %4$s%5$s',
    t.tgname,
    info.action_timing,
    array_to_string(info.action_statement, '/'),
    info.action_orientation,
    (
      case when info.action_condition is not null
        then '\n(conditional)'
        else ''
      end
    )
  ) as label
from pg_class as c
join pg_trigger as t on t.tgrelid = c.oid
join pg_proc as p on p.oid = t.tgfoid
join info on info.tgname = t.tgname
where c.oid = :name::text::regclass
  and p.pronamespace <> 'pg_catalog'::regnamespace
  and (
    :op::text = 'all'
    or info.action_statement @> array[:op]
  );

-- name: grants?
-- retrieve own permissions and permissions granted through inherited roles.
-- param: username: &str
with recursive roles as (
  select
    1 as level,
    oid as roleid,
    rolname,
    null::name as grantee,
    'self'::name as grantor
  from pg_roles
  where rolname = :username
  union
  select
    2,
    parent.oid,
    parent.rolname,
    roles.rolname,
    grantor.rolname
  from roles
  join pg_auth_members as am on am.member = roles.roleid
  join pg_roles as parent on parent.oid = am.roleid
  join pg_roles as grantor on grantor.oid = am.grantor
), grants as (
  select
    c.relname,
    n.nspname,
    acls.*
  from pg_class as c
  join pg_namespace as n on n.oid = c.relnamespace
  join lateral aclexplode(relacl) as acls (grantor, grantee, privileges, is_grantable) on true
), edges as (
  select
    r.grantee as "from",
    r.rolname as "to",
    null as label,
    'roles' as subgraph
  from roles as r
  where grantee is not null
  union
  select
    r.rolname,
    g.relname,
    string_agg(lower(g.privileges), '\n' order by g.privileges),
    null
  from roles as r
  left outer join grants as g on g.grantee = r.roleid
  group by r.rolname, g.relname
)
select "from", "to", label, subgraph
from edges
where "from" is not null
  and "to" is not null;
