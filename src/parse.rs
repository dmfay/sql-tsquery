use tree_sitter::{Parser, Query, QueryCursor, Tree, StreamingIterator};

use crate::qualname::Qualname;

/*
 * e.g.:
 * --filter insert --query "((object_reference name: (identifier) @tbl))"
 * --filter select --query "((invocation name: (identifier) @tbl))"
 */

#[derive(Debug)]
pub struct TSQuery {}

impl TSQuery {
    fn parse(source_code: String) -> Option<Tree> {
        let mut parser = Parser::new();

        parser
            .set_language(&tree_sitter_sequel::LANGUAGE.into())
            .expect("Error loading sql grammar");

        parser.parse(source_code, None)
    }

    pub fn get_text(query: &str, source_code: &str) -> Vec<String> {
        let tsquery = Query::new(&tree_sitter_sequel::LANGUAGE.into(), query).unwrap();
        let tree = Self::parse(source_code.to_string()).unwrap();
        let mut cur = QueryCursor::new();
        let mut matches: Vec<String> = vec![];
        let mut iter = cur.matches(&tsquery, tree.root_node(), source_code.as_bytes());

        while let Some(m) = iter.next() {
            let capture = m.captures[0].node;
            let text = source_code.get(capture.start_byte()..capture.end_byte());

            if text.is_some() {
                matches.push(text.unwrap().to_string());
            }
        }

        matches
    }

    pub fn get_matches(filter: &str, query: &str, source_code: &str) -> Vec<Qualname> {
        let tree = Self::parse(source_code.to_string()).unwrap();
        let mut cur = QueryCursor::new();
        let mut names: Vec<Qualname> = vec![];

        if !filter.is_empty() {
            let filter_query = Query::new(
                &tree_sitter_sequel::LANGUAGE.into(),
                &format!("((statement ({})) @statement)", filter),
            )
            .unwrap();

            let mut iter = cur.matches(&filter_query, tree.root_node(), source_code.as_bytes());

            // first home in on the specific subtrees of a potentially multi-statement program that
            // match the DML statement we're interested in
            while let Some(statement) = iter.next() {
                let tsquery = Query::new(&tree_sitter_sequel::LANGUAGE.into(), query).unwrap();
                let mut cur = QueryCursor::new();
                let matches =
                    cur.matches(&tsquery, statement.captures[0].node, source_code.as_bytes());

                // then run the inner query to retrieve the names of involved objects
                names.append(
                    &mut matches
                        .map_deref(|m| Qualname::from_match(source_code, m))
                        .collect(),
                );
            }
        } else {
            let tsquery = Query::new(&tree_sitter_sequel::LANGUAGE.into(), query).unwrap();
            let mut cur = QueryCursor::new();
            let matches = cur.matches(&tsquery, tree.root_node(), source_code.as_bytes());

            names.append(
                &mut matches
                    .map_deref(|m| Qualname::from_match(source_code, m))
                    .collect(),
            );
        }

        names
    }

    pub fn get_error_ratio(source_code: &str) -> f64 {
        let tree = Self::parse(source_code.to_string()).unwrap();
        let mut cur = QueryCursor::new();
        let tsquery = Query::new(&tree_sitter_sequel::LANGUAGE.into(), "((ERROR) @error)").unwrap();
        let errors = cur.matches(&tsquery, tree.root_node(), source_code.as_bytes());

        let mut pos = 0;

        // count bytes under potentially overlapping error matches by marking
        // the latest error end position
        let errlen = errors.fold(0, |mut acc, e| {
            let start = e.captures[0].node.range().start_byte;
            let end = e.captures[0].node.range().end_byte;

            if end > pos {
                if start < pos {
                    acc = acc + end - pos;
                }

                acc = acc + end - start;

                pos = end;
            }

            acc
        });

        100.0 * (1.0 - errlen as f64 / source_code.len() as f64)
    }
}
