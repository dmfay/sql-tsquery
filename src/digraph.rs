use std::collections::{HashMap, HashSet};

use crate::qualname::Qualname;
use log::warn;

#[derive(Default, Debug, Eq, Hash, PartialEq)]
pub struct Edge {
    from: Qualname,
    to: Qualname,
    label: Option<String>,
    style: Option<String>,
    dir: Option<String>,
    from_parent: bool,
}

#[derive(Default, Debug, Eq, Hash, PartialEq)]
pub struct Node {
    pub name: Qualname,
    parsed: Option<i32>,
}

#[derive(Default, Debug)]
pub struct Digraph {
    name: String,
    options: HashMap<String, String>,
    pub nodes: HashSet<Node>, // prevent duplication of nodes
    pub edges: Vec<Edge>,     // but allow multiple identical edges
    pub subgraphs: Vec<Digraph>,
    is_cluster: bool,
}

impl Digraph {
    pub fn new(name: String) -> Self {
        Digraph {
            name,
            // nodes: HashSet::new(),
            // edges: vec!(),
            // subgraphs: vec!(),
            ..Default::default()
        }
    }

    pub fn new_cluster(name: String) -> Self {
        Digraph {
            name,
            is_cluster: true,
            ..Default::default()
        }
    }

    pub fn options(&mut self, options: HashMap<String, String>) -> &mut Self {
        self.options = options;

        self
    }

    pub fn extend(mut self, other: Digraph) -> Digraph {
        self.nodes.extend(other.nodes);
        self.edges.extend(other.edges);
        self.subgraphs.extend(other.subgraphs);

        self
    }

    pub fn print(&self, format: &String, elide_schema: &Option<String>) {
        if self.nodes.is_empty() && self.edges.is_empty() && self.subgraphs.is_empty() {
            warn!("graph {} is empty!", self.name);
        }

        let ancestry = vec![];

        if format == "mermaid" {
            println!(
                "{}",
                <Digraph as Mermaid>::display(&self, 1, &ancestry, elide_schema)
            );
        } else {
            println!(
                "{}",
                <Digraph as Dot>::display(&self, 1, &ancestry, elide_schema)
            );
        }

        return;
    }

    pub fn has_edge(&self, node: Qualname, label_filter: &dyn Fn(&Option<String>) -> bool) -> bool {
        // direction-insensitive!
        return self.edges.iter().any(|edge| {
            (edge.from == Qualname::from(node.to_string())
                || edge.to == Qualname::from(node.to_string()))
                && label_filter(&edge.label)
        });
    }
}

impl Edge {
    pub fn new(from: Qualname, to: Qualname) -> Self {
        Edge {
            from,
            to,
            ..Default::default()
        }
    }

    pub fn new_from_parent(from: Qualname, to: Qualname) -> Self {
        Edge {
            from,
            to,
            from_parent: true,
            ..Default::default()
        }
    }

    pub fn label(&mut self, label: String) -> &mut Self {
        self.label = Some(label);

        self
    }

    pub fn style(&mut self, style: String) -> &mut Self {
        self.style = Some(style);

        self
    }

    pub fn dir(&mut self, dir: String) -> &mut Self {
        self.dir = Some(dir);

        self
    }

    pub fn prefix(&self, ancestry: &[String], from: bool) -> String {
        let name = match from {
            true => &self.from,
            false => &self.to,
        };
        let mut v = ancestry[0..].to_vec();

        if from && self.from_parent {
            // if this is the entry point from the immediate parent graph,
            // exclude the current graph name so the edge's `from` is properly
            // an outer-graph node
            v.pop();
        }

        v.push(name.to_string());

        v.join("_").to_string()
    }
}

impl Node {
    pub fn new(name: Qualname) -> Self {
        Node {
            name,
            ..Default::default()
        }
    }

    pub fn new_parsed(name: Qualname, parsed: i32) -> Self {
        Node {
            name,
            parsed: Some(parsed),
        }
    }

    pub fn prefix(&self, ancestry: &[String]) -> String {
        match ancestry.len() {
            0 => self.name.to_string(),
            _ => format!("{}_{}", ancestry[0..].join("_"), self.name),
        }
    }
}

trait Dot {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String;
}

impl Dot for Digraph {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String {
        let mut current = ancestry.clone();

        if self.is_cluster {
            // only track some graphs in ancestry; in particular we want to avoid including
            // the outer "operation" graph and the nested "roles" graph for grants
            current.push(self.name.to_string());
        }

        let indent = depth * 2;
        let mut lines = vec![];

        if self.options.contains_key("rankdir") {
            lines.push(format!(
                "{:indent$}rankdir={}",
                "",
                self.options.get("rankdir").unwrap_or(&"TB".to_string())
            ));
        }

        for node in self.nodes.iter() {
            lines.push(<Node as Dot>::display(&node, depth, &current, elide_schema));
        }

        for edge in self.edges.iter() {
            lines.push(<Edge as Dot>::display(&edge, depth, &current, elide_schema));
        }

        for subgraph in self.subgraphs.iter() {
            lines.push(format!(
                "{:indent$}subgraph \"cluster_{}\" {{",
                "", subgraph.name
            ));
            lines.push(<Digraph as Dot>::display(
                &subgraph,
                depth + 1,
                &current,
                elide_schema,
            ));
            lines.push(format!("{:indent$}}}", ""));
        }

        lines.join("\n")
    }
}

impl Dot for Edge {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        _elide_schema: &Option<String>,
    ) -> String {
        let mut attrs = vec![];
        let indent = depth * 2;

        if let Some(label) = &self.label {
            attrs.push(format!("label=\"{}\"", label));
        }

        if let Some(style) = &self.style {
            attrs.push(format!("style=\"{}\"", style));
        }

        if let Some(dir) = &self.dir {
            attrs.push(format!("dir=\"{}\"", dir));
        }

        if attrs.len() > 0 {
            format!(
                "{:indent$}\"{}\" -> \"{}\" [{}]",
                "",
                self.prefix(&ancestry, true),
                self.prefix(&ancestry, false),
                attrs.join(" ")
            )
        } else {
            format!(
                "{:indent$}\"{}\" -> \"{}\"",
                "",
                self.prefix(&ancestry, true),
                self.prefix(&ancestry, false)
            )
        }
    }
}

impl Dot for Node {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String {
        let indent = depth * 2;

        if let Some(parsed) = &self.parsed {
            return format!(
                "{:indent$}\"{}\" [label=\"{}\\n({}% parsed)\"]",
                "",
                self.prefix(&ancestry),
                self.name.display(elide_schema.clone()),
                parsed
            );
        } else {
            format!(
                "{:indent$}\"{}\" [label=\"{}\"]",
                "",
                self.prefix(&ancestry),
                self.name.display(elide_schema.clone())
            )
        }
    }
}

trait Mermaid {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String;
}

impl Mermaid for Digraph {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String {
        let mut current = ancestry.clone();

        if self.is_cluster {
            // only track some graphs in ancestry; in particular we want to avoid including
            // the outer "operation" graph and the nested "roles" graph for grants
            current.push(self.name.to_string());
        }

        let indent = depth * 2;
        let mut lines = vec![];

        for node in self.nodes.iter() {
            lines.push(<Node as Mermaid>::display(
                &node,
                depth,
                &current,
                elide_schema,
            ));
        }

        for edge in self.edges.iter() {
            lines.push(<Edge as Mermaid>::display(
                &edge,
                depth,
                &current,
                elide_schema,
            ));
        }

        for subgraph in self.subgraphs.iter() {
            lines.push(format!(
                "{:indent$}subgraph \"cluster_{}\" [\"{}\"]",
                "", subgraph.name, subgraph.name
            ));
            lines.push(<Digraph as Mermaid>::display(
                &subgraph,
                depth + 1,
                &current,
                elide_schema,
            ));
            lines.push(format!("{:indent$}end", ""));
        }

        lines.join("\n")
    }
}

impl Mermaid for Edge {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        _elide_schema: &Option<String>,
    ) -> String {
        let indent = depth * 2;

        if let Some(label) = &self.label {
            format!(
                "{:indent$}{}-->|\"{}\"|{}",
                "",
                self.prefix(&ancestry, true),
                label,
                self.prefix(&ancestry, false)
            )
        } else {
            format!(
                "{:indent$}{}-->{}",
                "",
                self.prefix(&ancestry, true),
                self.prefix(&ancestry, false)
            )
        }
    }
}

impl Mermaid for Node {
    fn display(
        &self,
        depth: usize,
        ancestry: &Vec<String>,
        elide_schema: &Option<String>,
    ) -> String {
        let indent = depth * 2;

        if let Some(parsed) = &self.parsed {
            return format!(
                "{:indent$}{}[\"{}\\n({}% parsed)\"]",
                "",
                self.prefix(&ancestry),
                self.name.display(elide_schema.clone()),
                parsed,
            );
        } else {
            format!(
                "{:indent$}{}[\"{}\"]",
                "",
                self.prefix(&ancestry),
                self.name.display(elide_schema.clone())
            )
        }
    }
}
