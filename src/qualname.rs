use std::fmt;
use std::fmt::Display;
use tree_sitter::QueryMatch;

#[derive(Debug, Hash, Eq, PartialEq, Clone)]
pub struct Qualname {
    pub schema: Option<String>,
    pub object: String,
}

impl Qualname {
    pub fn schema(&self) -> String {
        self.schema.clone().unwrap_or("public".to_string())
    }

    pub fn from_match(source_code: &str, m: &QueryMatch) -> Self {
        let start = m.captures[0].node.range().start_byte;
        let end = m.captures[m.captures.len() - 1].node.range().end_byte;

        Self::from(source_code[start..end].to_string())
    }

    pub fn from_names(schema: Option<&str>, object: &str) -> Self {
        Qualname {
            schema: schema.map(String::from),
            object: object.to_string(),
        }
    }

    pub fn to_string(&self) -> String {
        if self.schema.is_some() {
            return format!("{}.{}", self.schema.as_ref().unwrap(), self.object);
        }

        self.object.to_string()
    }

    pub fn display(&self, elide_schema: Option<String>) -> String {
        if self.schema == elide_schema {
            return self.object.to_string();
        }

        self.to_string()
    }
}

impl From<String> for Qualname {
    fn from(qualname: String) -> Self {
        let mut iter = qualname.rsplitn(2, '.');
        let object = iter.next().unwrap();
        let schema = iter.next().map(String::from);

        Qualname {
            schema,
            object: object.to_string(),
        }
    }
}

impl Default for Qualname {
    fn default() -> Self {
        Qualname {
            schema: None,
            object: "".to_string(),
        }
    }
}

impl Display for Qualname {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.to_string())
    }
}
