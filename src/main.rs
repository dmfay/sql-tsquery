use std::collections::HashSet;
use std::env;
use std::process;

use log::error;

use pdot::{
    create_client, plot_all_fks, plot_fks, plot_func_refs, plot_grants, plot_triggers, plot_views,
    BoxedError, Qualname,
};

#[derive(Debug)]
struct Args {
    format: String,
    postgraphile: bool,
    dbname: String,
    opname: String,
    objs: Vec<String>,
    attname: String,
    username: String,
}

fn help() {
    println!(
        r#"
pdot generates GraphViz dot or mermaid definitions for a number of interesting
directed graphs in PostgreSQL databases. The output is not a complete digraph
but is intended to be interpolated into dot's digraph {{}} directive, especially
as a shell function, or after mermaid's flowchart header.

Recommended usage is in a shell function that performs interpolation into a dot
template and pipes output to an image renderer. See README.md for examples.

Flags:

--format=<dot, mermaid> - output different diagram types (default dot).
--postgraphile          - enable Postgraphile @foreignKey smart comment
                          processing.

Available operations:

- pdot dbname fks

    Plot the foreign key graph for the entire database. Use with care: this can
    be very large and GraphViz is only so good at untangling huge graphs!

- pdot dbname fks [schema.]table

    Plot the graph of tables linked to `table` by lineal foreign keys (that is,
    no peeking around corners: given a -> b <- c, starting from a plots a and
    b, starting from c plots b and c, starting from b plots all three).

- pdot dbname views [schema.]relation [column]

    Plot recursive references to `relation` (a table or view) in views.
    Optionally filter for references to a specific column.

- pdot dbname refs [schema.]function

    Plot recursive references made by `function`, including functions it
    invokes, relations it interacts with, and triggers resulting from those
    relation interactions.

- pdot dbname triggers [schema.]table

    Plot triggers on `table` and the functions and relations they reference
    recursively.

- pdot dbname grants username

    Plot user role inheritance and permissions on objects in the current
    database."#
    );
    process::exit(0);
}

fn parse_args(
    raw: impl IntoIterator<Item = impl Into<std::ffi::OsString>>,
) -> Result<Args, BoxedError> {
    let mut args = Args {
        format: "dot".to_string(),
        postgraphile: false,
        dbname: "".to_string(),
        opname: "".to_string(),
        objs: vec![],
        attname: "".to_string(),
        username: "".to_string(),
    };

    let raw = clap_lex::RawArgs::new(raw);
    let mut cursor = raw.cursor();
    raw.next(&mut cursor); // skip the binary itself
    while let Some(arg) = raw.next(&mut cursor) {
        if let Some((long, value)) = arg.to_long() {
            match long {
                Ok("format") => {
                    args.format = value.unwrap().to_str().unwrap().to_string();
                }
                Ok("postgraphile") => {
                    args.postgraphile = true;
                }
                Ok("help") => help(),
                _ => {
                    return Err(format!("Illegal argument: --{}", arg.display()).into());
                }
            }
        } else if let Some(mut shorts) = arg.to_short() {
            while let Some(short) = shorts.next_flag() {
                match short {
                    Ok('h') => help(),
                    _ => {
                        return Err(format!("Illegal argument: -{}", arg.display()).into());
                    }
                }
            }
        } else if args.dbname.is_empty() {
            args.dbname = arg.to_value_os().to_owned().into_string().unwrap();
        } else if args.opname.is_empty() {
            args.opname = arg.to_value_os().to_owned().into_string().unwrap();
        } else {
            match args.opname.as_str() {
                "grants" => {
                    // one username, 0 or more qualified object names
                    if args.username.is_empty() {
                        args.username = arg.to_value_os().to_owned().into_string().unwrap();
                    } else {
                        args.objs
                            .push(arg.to_value_os().to_owned().into_string().unwrap());
                    }
                }
                _ => {
                    // one qualified object name, one optional attribute name
                    if args.objs.is_empty() {
                        args.objs
                            .push(arg.to_value_os().to_owned().into_string().unwrap());
                    } else if args.attname.is_empty() {
                        args.attname = arg.to_value_os().to_owned().into_string().unwrap();
                    }
                }
            }
        }
    }

    Ok(args)
}

fn main() {
    stderrlog::new()
        .verbosity(1)
        .module(module_path!())
        .init()
        .unwrap(); // print warnings to stderr

    let args: Vec<String> = env::args().collect();
    let parsed: Args = match parse_args(args) {
        Ok(p) => p,
        Err(err) => {
            error!("{}", err);

            return;
        }
    };

    let host = env::var("PGHOST").unwrap_or("localhost".to_string());
    let port = env::var("PGPORT").unwrap_or("5432".to_string()).parse::<i32>().unwrap();
    let user = env::var("PGUSER").unwrap_or("postgres".to_string());
    let pass = env::var("PGPASSWORD").ok();

    let client = create_client(&parsed.dbname, host, port, user, pass);

    let qualname: Option<Qualname> = if !parsed.objs.is_empty() {
        Some(Qualname::from(parsed.objs[0].clone()))
    } else {
        None
    };

    let digraph = match parsed.opname.as_str() {
        "fks" => match parsed.objs.len() {
            0 => plot_all_fks(&mut client.unwrap(), parsed.postgraphile),
            _ => plot_fks(
                &mut client.unwrap(),
                parsed.postgraphile,
                qualname.clone().unwrap(),
            ),
        },
        "views" => {
            if parsed.objs.is_empty() {
                error!("must specify target view");

                return;
            }

            plot_views(
                &mut client.unwrap(),
                qualname.clone().unwrap(),
                parsed.attname.as_str(),
            )
        }
        "refs" => {
            if parsed.objs.is_empty() {
                error!("must specify target function");

                return;
            }

            plot_func_refs(
                &mut client.unwrap(),
                qualname.clone().unwrap(),
                &mut HashSet::new(),
            )
        }
        "triggers" => {
            if parsed.objs.is_empty() {
                error!("must specify target table");

                return;
            }

            plot_triggers(
                &mut client.unwrap(),
                qualname.clone().unwrap(),
                &mut HashSet::new(),
                None,
            )
        }
        "grants" => plot_grants(&mut client.unwrap(), parsed.username.as_str(), parsed.objs),
        "" => {
            help();

            return;
        }
        _ => {
            error!("invalid operation: {}", parsed.opname);

            return;
        }
    };

    match digraph {
        Ok(digraph) => {
            if let Some(q) = qualname {
                digraph.print(&parsed.format, &q.schema);
            } else {
                digraph.print(&parsed.format, &None);
            }
        }
        Err(err) => {
            error!("{}", err);
        }
    }
}
