use std::collections::{HashMap, HashSet};

use include_postgres_sql::{impl_sql, include_sql};
use log::warn;
use postgres::fallible_iterator::FallibleIterator;
use postgres::{Client, NoTls, Row};

use digraph::{Digraph, Edge, Node};
use parse::TSQuery;
pub use qualname::Qualname;

mod digraph;
mod parse;
mod qualname;

pub type BoxedError = Box<dyn std::error::Error + Send + Sync>;

include_sql!("sql/postgres.sql");

pub fn create_client(dbname: &str, host: String, port: i32, user: String, pass: Option<String>) -> Result<Client, postgres::Error> {
    let conn = match pass {
        None => format!(
            "host={} port={} user={} dbname={}",
            host, port, user, dbname
        ),
        Some(val) => format!(
            "host={} port={} user={} password={} dbname={}",
            host, port, user, val, dbname
        ),
    };

    Client::connect(conn.as_str(), NoTls)
}

pub fn plot_all_fks(client: &mut Client, postgraphile: bool) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("fks".to_string());

    client.all_fks("public", |row| {
        graph.edges.push(Edge::new(
            Qualname::from(row.get::<usize, String>(0)),
            Qualname::from(row.get::<usize, String>(1)),
        ));

        Ok(())
    })?;

    if postgraphile {
        client.all_fks_postgraphile("public", |row| {
            let mut edge = Edge::new(
                Qualname::from(row.get::<usize, String>(0)),
                Qualname::from(row.get::<usize, String>(1)),
            );

            edge.style("dotted".to_string());
            edge.label("@foreignKey".to_string());

            graph.edges.push(edge);

            Ok(())
        })?;
    }

    client.all_inheritors("public", |row| {
        let mut edge = Edge::new(
            Qualname::from(row.get::<usize, String>(0)),
            Qualname::from(row.get::<usize, String>(1)),
        );

        edge.label("inherits".to_string());
        edge.style("dashed".to_string());
        edge.dir("back".to_string());

        graph.edges.push(edge);

        Ok(())
    })?;

    Ok(graph)
}

pub fn plot_fks(
    client: &mut Client,
    postgraphile: bool,
    qualname: Qualname,
) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("fks".to_string());

    if postgraphile {
        client.fks_postgraphile(&qualname.schema(), &qualname.object, |row| {
            let mut edge = Edge::new(
                Qualname::from(row.get::<usize, String>(0)),
                Qualname::from(row.get::<usize, String>(1)),
            );

            if row.get::<usize, String>(2) == "postgraphile" {
                edge.style("dotted".to_string());
                edge.label("@foreignKey".to_string());
            }

            graph.edges.push(edge);

            Ok(())
        })?;
    } else {
        client.fks(&qualname.schema(), &qualname.object, |row| {
            let edge = Edge::new(
                Qualname::from(row.get::<usize, String>(0)),
                Qualname::from(row.get::<usize, String>(1)),
            );

            graph.edges.push(edge);

            Ok(())
        })?;
    }

    client.all_inheritors(&qualname.schema(), |row| {
        // only show inheritance relationships involving our selected fk subgraph
        if graph.has_edge(Qualname::from(row.get::<usize, String>(0)), &|label| {
            label != &Some("inherits".to_string())
        }) || graph.has_edge(Qualname::from(row.get::<usize, String>(1)), &|label| {
            label != &Some("inherits".to_string())
        }) {
            let mut edge = Edge::new(
                Qualname::from(row.get::<usize, String>(0)),
                Qualname::from(row.get::<usize, String>(1)),
            );

            edge.label("inherits".to_string());
            edge.style("dashed".to_string());
            edge.dir("back".to_string());

            graph.edges.push(edge);
        }

        Ok(())
    })?;

    Ok(graph)
}

pub fn plot_views(
    client: &mut Client,
    qualname: Qualname,
    attname: &str,
) -> Result<Digraph, BoxedError> {
    if !attname.is_empty() {
        return plot_view_attrs(client, qualname, attname);
    }

    let mut graph = Digraph::new("views".to_string());

    client.views(&qualname.schema(), &qualname.object, |row| {
        graph.edges.push(Edge::new(
            Qualname::from(row.get::<usize, String>(0)),
            Qualname::from(row.get::<usize, String>(1)),
        ));

        Ok(())
    })?;

    Ok(graph)
}

fn plot_view_attrs(
    client: &mut Client,
    qualname: Qualname,
    attname: &str,
) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("views".to_string());

    client.view_attrs(&qualname.schema(), &qualname.object, attname, |row| {
        graph.edges.push(Edge::new(
            Qualname::from(row.get::<usize, String>(0)),
            Qualname::from(row.get::<usize, String>(1)),
        ));

        Ok(())
    })?;

    Ok(graph)
}

pub fn plot_func_refs(
    client: &mut Client,
    qualname: Qualname,
    seen: &mut HashSet<Qualname>,
) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("functions".to_string());

    let row = client.function_src(&qualname.to_string())?;
    let source_code = row.get(0);

    graph.nodes.insert(Node::new_parsed(
        qualname.clone(),
        TSQuery::get_error_ratio(source_code).round() as i32,
    ));

    seen.insert(qualname.clone());

    for (i, statement) in TSQuery::get_text("((statement) @statement)", source_code)
        .into_iter()
        .skip(1) // first result is the `create function` itself
        .enumerate()
    {
        let mut subgraph = Digraph::new_cluster(format!("{}_{}", qualname.to_string(), i));
        let mut first: Option<Qualname> = None;

        for (verb, query) in HashMap::from([
          ("select", "((select) (from (relation (object_reference schema: (identifier)? @schema name: (identifier) @name))))"),
          ("insert", "(insert (object_reference schema: (identifier)? @schema name: (identifier) @name))"),
          ("update", "(update (relation (object_reference schema: (identifier)? @schema name: (identifier) @name)))"),
          ("delete", "((delete) (from (object_reference schema: (identifier)? @schema name: (identifier) @name)))")
        ]) {
            for stmt_qualname in TSQuery::get_matches(verb, query, &statement)
                .into_iter()
                .filter(|t| !seen.contains(t))
                .collect::<Vec<Qualname>>()
            {
                let recurs = plot_triggers(client, stmt_qualname.clone(), seen, Some(verb.to_string()));

                if recurs.is_ok() {
                    subgraph = subgraph.extend(recurs.unwrap());
                } else {
                    warn!("{}", recurs.err().unwrap());
                }

                // create a named node for the statement's primary target
                // and link it to the function's qualname; hang onto a ref
                // so we can link in-statement function invocations
                if first.is_none() {
                    first = Some(stmt_qualname.clone());
                }

                subgraph.nodes.insert(Node::new(stmt_qualname.clone()));

                let mut edge = Edge::new_from_parent(qualname.clone(), stmt_qualname.clone());

                edge.label(verb.to_string());

                subgraph.edges.push(edge);

                // get any other object_references involved in the statement
                // such as joined relations, selects feeding inserts, etc
                for sub in TSQuery::get_matches(
                    "",
                    "(relation (object_reference schema: (identifier)? @schema name: (identifier) @name))",
                    &statement
                )
                    .into_iter()
                    .filter(|s| s != &stmt_qualname)
                    .collect::<HashSet<Qualname>>() { // deduplicate
                        subgraph.nodes.insert(Node::new(sub.clone()));

                        let mut edge = Edge::new(stmt_qualname.clone(), sub);

                        edge.style("dotted".to_string());
                        edge.dir("none".to_string());

                        subgraph.edges.push(edge);
                }
            }
        }

        let invocations: Vec<Qualname> = TSQuery::get_matches("", "(invocation (object_reference schema: (identifier)? @schema name: (identifier) @name))", &statement)
            .into_iter()
            .filter(
                |name| !seen.contains(name) && ![
                    "any",
                    "array_agg",
                    "avg",
                    "coalesce",
                    "count",
                    "cume_dist",
                    "dense_rank",
                    "first_value",
                    "json_agg",
                    "jsonb_agg",
                    "jsonb_object_agg",
                    "lag",
                    "last_value",
                    "lead",
                    "max",
                    "min",
                    "nth_value",
                    "ntile",
                    "percent_rank",
                    "rank",
                    "row_number",
                    "string_agg",
                    "sum",
                    "now",
                ].contains(&name.to_string().as_str())
            )
            .collect();

        // mark all invocations seen here so they get attached at this
        // point in the graph rather than potentially when we recurse
        seen.extend(invocations.iter().cloned());

        for inv in invocations {
            let recurs = plot_func_refs(client, inv.clone(), seen);

            if recurs.is_ok() {
                subgraph = subgraph.extend(recurs.unwrap());
            } else {
                subgraph.nodes.insert(Node::new_parsed(inv.clone(), 0));

                warn!("{}", recurs.err().unwrap());
            }

            if first.is_some() {
                let mut edge = Edge::new(first.clone().unwrap(), inv);

                edge.style("dotted".to_string());
                edge.dir("none".to_string());

                subgraph.edges.push(edge);
            } else {
                let mut edge = Edge::new(inv.clone(), inv);

                edge.style("dotted".to_string());
                edge.dir("none".to_string());

                subgraph.edges.push(edge);
            }
        }

        graph.subgraphs.push(subgraph);
    }

    Ok(graph)
}

pub fn plot_triggers(
    client: &mut Client,
    qualname: Qualname,
    seen: &mut HashSet<Qualname>,
    op: Option<String>,
) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("triggers".to_string());

    // if `nodes` were a vec this would duplicate on recursive invocations
    // because the table node is added by plot_func_refs; however, the very
    // first table target would _lack_ a named node, and so would retain
    // its schema on the graph instead of eliding correctly
    graph.nodes = vec![Node::new(qualname.clone())].into_iter().collect();

    let triggers: Vec<postgres::Row> = client
        .triggers(&qualname.to_string(), &op.unwrap_or("all".to_string()))?
        .filter(|row: &Row| {
            if !seen.contains(&Qualname {
                schema: Some(row.get(0)),
                object: row.get(1),
            }) {
                Ok(true)
            } else {
                Ok(false)
            }
        })
        .collect()?;

    // mark *all* discovered triggers seen so they don't risk attachment at
    // other graph locations when we recurse
    seen.extend(
        triggers
            .iter()
            .map(|row| Qualname {
                schema: row.get(0),
                object: row.get(1),
            })
            .collect::<Vec<Qualname>>(),
    );

    // but still track trigger functions we're iterating over, since we don't
    // want to reprocess the same function if it's invoked by multiple triggers
    let mut local_seen: HashSet<Qualname> = Default::default();

    for row in triggers {
        let schema: &str = row.get(0);
        let function: &str = row.get(1);
        let label: &str = row.get(2);
        let qualfn = Qualname::from_names(Some(schema), function);

        let mut edge = Edge::new(qualname.clone(), qualfn.clone());

        edge.label(label.to_string());
        edge.style("dashed".to_string());

        graph.edges.push(edge);

        if local_seen.contains(&qualfn) {
            continue;
        }

        local_seen.insert(qualfn.clone());

        let recurs = plot_func_refs(client, qualfn, seen);

        if recurs.is_ok() {
            graph = graph.extend(recurs.unwrap());
        } else {
            warn!("{}", recurs.err().unwrap());
        }
    }

    Ok(graph)
}

// TODO restrict to list of qualnames in objs
pub fn plot_grants(
    client: &mut Client,
    username: &str,
    _objs: Vec<String>,
) -> Result<Digraph, BoxedError> {
    let mut graph = Digraph::new("grants".to_string());

    graph.options(HashMap::from([("rankdir".to_string(), "LR".to_string())]));

    graph.subgraphs = vec![Digraph::new("roles".to_string())];

    client.grants(username, |row| {
        match row.get(3) {
            Some("roles") => {
                let mut edge = Edge::new(
                    Qualname::from(row.get::<usize, String>(0)),
                    Qualname::from(row.get::<usize, String>(1)),
                );

                edge.label("inherits".to_string());
                edge.style("dashed".to_string());

                graph.subgraphs[0].edges.push(edge)
            }
            None => {
                let mut edge = Edge::new(
                    Qualname::from(row.get::<usize, String>(0)),
                    Qualname::from(row.get::<usize, String>(1)),
                );

                edge.label(row.get(2));

                graph.edges.push(edge)
            }
            _ => {}
        }

        Ok(())
    })?;

    Ok(graph)
}
