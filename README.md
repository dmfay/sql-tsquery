# pdot

pdot produces [GraphViz](https://graphviz.org) `dot` (or [mermaid](https://mermaid.js.org)!) definitions for a number of interesting directed graphs implicit in [PostgreSQL](https://www.postgresql.org) databases. The output is not a complete dot digraph but is intended to be interpolated into dot's `digraph {}` directive, especially as a shell function, or after mermaid's `flowchart` header.

> ![partial foreign key graph from the `passenger` table](doc/pgair-fks-passenger.png)
>
> *Foreign keys relevant to the `passenger` table in a modified [Postgres Air](https://github.com/hettie-d/postgres_air) database*

> ![trigger flow from the `flight` table showing cascading effects of a flight delay on other flights and passenger bookings](doc/pgair-triggers-flight.png)
>
> *Trigger flows starting from the `flight` table*

> ![mermaid diagram of inherited roles and grants for the `pgair_agent` role](doc/pgair-grants-agent-mermaid.png)
>
> *Mermaid diagram showing inherited roles and grants for the `pgair_agent` role*

Most of pdot's digraphs represent a perspective on the database from the vantage point of a single table, function, or role. This facilitates a more exploratory approach, generating many _disposable_, more focused graphs to "move" your perspective around the database as detailed [here](https://di.nmfay.com/exploring-databases-visually):

> Documentation isn't the only tool we have for orienting ourselves in a system: we can also explore, view the system in parts and from different angles, follow individual paths through the model from concept to concept. Exploration depends on adopting a partial, mobile perspective from the inside of the model, with rapid feedback and enough context to navigate but not so much as to be overwhelmed. The view from a single point is more or less important depending on the point itself, but in order to facilitate exploration that view has to be generated and discarded on demand. Look, move, look, move.

Use of an image-capable terminal emulator is highly recommended!

## Installation & Use

### Prerequisites

- **GraphViz** is probably already available from your package manager of choice, if you're using Linux or OSX.

### Download

- **Arch Linux**: install `pdot-git` from the AUR.
- **Linux**, **Windows**, **macOS**: download the latest version from the [releases page](https://gitlab.com/dmfay/pdot/-/releases).
- Building from source:

```sh
# clone
git clone git@gitlab.com:dmfay/pdot.git

# return to source directory and build
cd .. && cargo build --release
```

### Install

`pdot` should be on your `PATH` for convenience. If you've downloaded the binary, you'll probably need to move it to a directory that's already on your `PATH` like /usr/bin.

By itself, the `pdot` binary will output the _body_ of a dot `digraph` or a Mermaid `flowchart`. To produce images interactively, you need to install a shell function which will interpolate that output into a template and pipe the generated image into your terminal's image-rendering command or to a file.

#### Zsh and [wezterm](https://wezfurlong.org/wezterm/)

<details>
<summary>Add this to your .zshrc, or to a file `source`d from there</summary>

```zsh
function pdot() {
  if [[ "$#" = 0 || "$@" = "--help" || "$@" = "-h" ]]; then
    command pdot "$@"
    return 0
  fi

  # customize to taste: https://graphviz.org/doc/info/attrs.html
  local DIGRAPH_TEMPLATE='
    digraph {
      node [
        shape="rect"
      ];
      concentrate=true;

      %s
    }
  '

  # run the _actual_ pdot
  local DOT_DEFN=$(command pdot "$@")

  if [ -t 1 ]; then
    # we're sending output to the terminal
    printf "$DIGRAPH_TEMPLATE" "$DOT_DEFN" | dot -Tpng | wezterm imgcat
  else
    # we're in a pipe
    printf "$DIGRAPH_TEMPLATE" "$DOT_DEFN" | dot -Tpng
  fi
}

# tab-completions
function _pdot() {
  local curcontext="$curcontext" state line
  local dbquery="select datname from pg_database"
  local ops="fks views refs triggers grants"
  local tblquery="select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.tables where table_schema not in ('pg_catalog', 'information_schema') and table_type = 'BASE TABLE' order by 1"
  local viewquery="select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.tables where table_schema not in ('pg_catalog', 'information_schema') union select case when table_schema <> 'public' then table_schema || '.' || table_name else table_name end from information_schema.views where table_schema not in ('pg_catalog', 'information_schema') order by 1"
  local fnquery="select case when routine_schema <> 'public' then routine_schema || '.' || routine_name else routine_name end from information_schema.routines where routine_schema not in ('pg_catalog', 'information_schema') order by 1"
  local usrquery="select rolname from pg_roles"
  typeset -A opt_args

  _arguments -C \
    "1:database:->db" \
    "2:op:->op" \
    "3:arg:->arg"

  case "$state" in
    db)
      _alternative "databases:database:($(psql --no-psqlrc --tuples-only -c $dbquery))"
      ;;
    op)
      _alternative "ops:op:($ops)"
      ;;
    arg)
      case $words[3] in
        (fks|triggers)
          _alternative "tables:table:($(psql $words[2] --no-psqlrc --tuples-only -c $tblquery 2>/dev/null))"
          ;;
        (views)
          _alternative "views:view:($(psql $words[2] --no-psqlrc --tuples-only -c $viewquery 2>/dev/null))"
          ;;
        (refs)
          _alternative "functions:function:($(psql $words[2] --no-psqlrc --tuples-only -c $fnquery 2>/dev/null))"
          ;;
        (grants)
          _alternative "roles:role:($(psql $words[2] --no-psqlrc --tuples-only -c $usrquery 2>/dev/null))"
          ;;
      esac
      ;;
  esac
}

compdef _pdot pdot
```
</details>

## Environment

- `PGHOST`: set the Postgres server hostname. Default `localhost`.
- `PGPORT`: set the Postgres server port. Default `5432`.
- `PGUSER`: set the Postgres connection username. Default `postgres`.
- `PGPASSWORD`: set the Postgres connection password. Not passed by default.

## Options

- `--format=<dot, mermaid>`: output different diagram types (default dot).
- `--postgraphile`: enable Postgraphile [`@foreignKey` smart comment](https://postgraphile.org/postgraphile/current/smart-tags/#foreignkey) processing.

## Available Operations

- `pdot dbname fks`

    Plot the foreign key graph for the entire database. Use with care: this can be very large and GraphViz is only so good at untangling huge graphs!

- `pdot dbname fks [schema.]table`

    Plot the graph of tables linked to `table` by lineal foreign keys (that is, no peeking around corners: given a -> b <- c, starting from a plots a and b, starting from c plots b and c, starting from b plots all three).

- `pdot dbname views [schema.]relation [column]`

    Plot recursive references to `relation` (a table or view) in views. Optionally filter for references to a specific column.

- `pdot dbname refs [schema.]function`

    Plot recursive references made by `function`, including functions it invokes, relations it interacts with, and triggers resulting from those relation interactions.

- `pdot dbname triggers [schema.]table`

    Plot triggers on `table` and the functions and relations they reference recursively.

- `pdot dbname grants username`

    Plot user role inheritance and permissions on objects in the current database.

## Caveats

- function body parsing in `triggers` and `refs` uses [tree-sitter-sql](https://github.com/DerekStride/tree-sitter-sql), which doesn't explicitly support PL/pgSQL but is usually pretty good at picking out DML statements and invocations.
