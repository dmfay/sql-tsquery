.PHONY: test

linux: target="x86_64-unknown-linux-gnu"
linux:
	rustup target add "${target}"
	cargo build --release "--target=${target}"
	mv "target/${target}/release/pdot" "target/pdot-${target}"

macos:
	targets="aarch64-apple-darwin x86_64-apple-darwin"
	# the `xcrun --show-sdk-path` invocation just before `cargo build` is
	# load-bearing: take it away and you get an error "invalid version number in
	# '-mmacosx-version-min='".
	for target in aarch64-apple-darwin x86_64-apple-darwin; do \
		rustup target add $$target ; \
		SDKROOT=$(xcrun --show-sdk-path) \
		MACOSX_DEPLOYMENT_TARGET=$(xcrun --show-sdk-platform-version) \
			xcrun --show-sdk-path && \
			cargo build --release "--target=$$target" ; \
	done

	# From: https://developer.apple.com/documentation/apple-silicon/building-a-universal-macos-binary#Update-the-Architecture-List-of-Custom-Makefiles
	lipo -create \
		-output target/pdot-universal-apple-darwin \
		target/aarch64-apple-darwin/release/pdot \
		target/x86_64-apple-darwin/release/pdot

win: target="x86_64-pc-windows-gnu"
win:
	rustup target add "${target}"
	cargo build --release "--target=${target}"
	mv "target/${target}/release/pdot.exe" "target/pdot-${target}.exe"

test:
	sh ./test.sh
