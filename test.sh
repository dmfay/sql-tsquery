#!/bin/bash
# pdot regression tests: run a bunch of standard commands and produce output,
# crudely sorted to avoid planner variance. compare diffs to see if anything
# changed.

if ! psql postgres_air -c "select 1" > /dev/null 2>&1; then
  echo "postgres_air database not detected!"
  echo ""
  echo "download postgres_air at https://github.com/hettie-d/postgres_air"

  exit 1
elif ! psql postgres_air -c "select 1 from postgres_air.credit limit 1" > /dev/null 2>&1; then
  echo "please apply the pdot additions to postgres_air!"
  echo ""
  echo "    psql postgres_air -f test/postgres-air-additions.sql"

  exit 1
fi

pdot_path="${1:-/usr/local/bin/pdot}"

function run_regression() {
  local args=( "$@" )
  local str="${args[*]}"
  local out="${str// /-}"
  local cmd=( "$pdot_path" "${args[@]}" )

  echo "running ${cmd[*]}"

  "${cmd[@]}" | sort > "test/regression/$out.dot"
}

# fks
run_regression postgres_air fks
run_regression postgres_air fks postgres_air.account
run_regression postgres_air fks postgres_air.airport
run_regression postgres_air fks postgres_air.booking
run_regression postgres_air fks postgres_air.booking_leg
run_regression postgres_air fks postgres_air.flight
run_regression postgres_air fks postgres_air.frequent_flyer
run_regression postgres_air fks postgres_air.employee
run_regression postgres_air fks postgres_air.passenger

# # views
run_regression postgres_air views postgres_air.aircraft
run_regression postgres_air views postgres_air.aircraft code
run_regression postgres_air views postgres_air.arriving_today
run_regression postgres_air views postgres_air.flight

# # triggers
run_regression postgres_air triggers postgres_air.booking
run_regression postgres_air triggers postgres_air.booking_leg
run_regression postgres_air triggers postgres_air.flight

# # function refs
run_regression postgres_air refs postgres_air.delay_flight

# # grants
run_regression postgres_air grants pgair_agent
run_regression postgres_air grants pgair_employee
run_regression postgres_air grants pgair_web
