-- Init

drop view if exists postgres_air.employee_trips_by_destination;
drop view if exists postgres_air.employee_trips;
drop view if exists postgres_air.aircraft_last_fueling;
drop view if exists postgres_air.daily_airport_activity;
drop view if exists postgres_air.arriving_today;
drop view if exists postgres_air.departing_today;
drop view if exists postgres_air.flight_monitor;

alter table postgres_air.account drop column if exists employee_id;
alter table postgres_air.aircraft drop column if exists capacity;
alter table postgres_air.airport drop column if exists location;
alter table postgres_air.flight drop column if exists arrival_gate_id;
alter table postgres_air.flight drop column if exists departure_gate_id;
alter table postgres_air.booking drop column if exists miles;
alter table postgres_air.account drop column if exists tier;

drop table if exists postgres_air.credit;
drop table if exists postgres_air.baggage_claim;
drop table if exists postgres_air.lost_and_found;
drop table if exists postgres_air.checked_bag;
drop table if exists postgres_air.provisioning;
drop table if exists postgres_air.supplier_airport;
drop table if exists postgres_air.supplier;
drop table if exists postgres_air.gate_agent;
drop table if exists postgres_air.gate;
drop table if exists postgres_air.standby;
drop table if exists postgres_air.flight_team_hospitality;
drop table if exists postgres_air.flight_team;
drop table if exists postgres_air.employee_pilot;
drop table if exists postgres_air.employee_hospitality;
drop table if exists postgres_air.employee_ground;
drop table if exists postgres_air.employee;
drop table if exists postgres_air.department;

-- Tables

create table postgres_air.credit (
  account_id int not null references postgres_air.account (account_id),
  amount int not null,
  awarded_at timestamptz not null default now()
);

create table postgres_air.baggage_claim (
  flight_id int not null primary key references postgres_air.flight (flight_id),
  airport_code char(3) references postgres_air.airport (airport_code),
  carousel_number int,
  started_at timestamptz,
  completed_at timestamptz
);

create table postgres_air.checked_bag (
  bag_id serial not null primary key,
  passenger_id int not null references postgres_air.passenger (passenger_id),
  from_flight_id int not null references postgres_air.flight (flight_id),
  destination_airport_code char(3) not null references postgres_air.airport (airport_code),
  checked_at timestamptz not null,
  weight int not null
);

create table postgres_air.lost_and_found (
  checked_bag_id serial not null primary key references postgres_air.checked_bag,
  airport_code char(3) not null references postgres_air.airport (airport_code),
  lost_on date not null,
  recovered_at timestamptz
);

create table postgres_air.supplier (
  supplier_id serial not null primary key,
  name text not null,
  provides text not null,
  contracted_on date not null,
  terminated_on date
);

create table postgres_air.supplier_airport (
  supplier_id int references postgres_air.supplier (supplier_id),
  airport_code char(3) references postgres_air.airport (airport_code),
  primary key (supplier_id, airport_code)
);

create table postgres_air.provisioning (
  supplier_id int not null references postgres_air.supplier (supplier_id),
  airport_code char(3) not null references postgres_air.airport (airport_code),
  aircraft_code text not null references postgres_air.aircraft (code),
  flight_id int not null references postgres_air.flight (flight_id),
  report text,
  supplied_at timestamptz not null,
  ok_to_fly boolean not null default false,
  primary key (supplier_id, airport_code, flight_id)
);

create table postgres_air.gate (
  gate_id serial not null primary key,
  airport_code char(3) not null references postgres_air.airport (airport_code),
  name text not null
);

create table postgres_air.department (
  department_id serial not null primary key,
  name text not null
);

create table postgres_air.employee (
  employee_id serial not null primary key,
  department_id int not null references postgres_air.department (department_id),
  first_name text not null,
  last_name text not null,
  home_airport char(3) not null references postgres_air.airport (airport_code),
  hired_on date not null,
  terminated_on date
);

create table postgres_air.employee_pilot (
  certification_number text not null,
  primary key (employee_id),
  check (department_id = 2)
) inherits (postgres_air.employee);

create table postgres_air.employee_hospitality (
  primary key (employee_id),
  check (department_id = 3)
) inherits (postgres_air.employee);

create table postgres_air.employee_ground (
  primary key (employee_id),
  check (department_id = 4)
) inherits (postgres_air.employee);

create table postgres_air.gate_agent (
  gate_id int not null references postgres_air.gate (gate_id),
  agent_id int not null references postgres_air.employee_ground (employee_id),
  on_date date not null,
  primary key (gate_id, agent_id, on_date)
);

create table postgres_air.flight_team (
  flight_id int not null references postgres_air.flight (flight_id) primary key,
  captain int not null references postgres_air.employee_pilot (employee_id),
  copilot int not null references postgres_air.employee_pilot (employee_id)
);

create table postgres_air.flight_team_hospitality (
  flight_id int not null references postgres_air.flight_team (flight_id),
  employee_id int not null references postgres_air.employee_hospitality (employee_id),
  primary key (flight_id, employee_id)
);

create table postgres_air.standby (
  flight_id int not null references postgres_air.flight (flight_id),
  passenger_id int not null references postgres_air.passenger (passenger_id),
  priority int not null
);

alter table postgres_air.account add column employee_id int references postgres_air.employee (employee_id);
alter table postgres_air.aircraft add column capacity int;
alter table postgres_air.airport add column location point;
alter table postgres_air.flight add column arrival_gate_id int references postgres_air.gate (gate_id);
alter table postgres_air.flight add column departure_gate_id int references postgres_air.gate (gate_id);
alter table postgres_air.booking add column miles int;
alter table postgres_air.account add column tier text;

-- Triggers

drop function if exists postgres_air.great_circle_time cascade;
drop function if exists postgres_air.schedule_arrival cascade;
drop function if exists postgres_air.flight_takeoff cascade;
drop function if exists postgres_air.delay_flight cascade;
drop function if exists postgres_air.rebook_next_leg cascade;
drop function if exists postgres_air.book_standby_check cascade;
drop function if exists postgres_air.booking_upgrade cascade;

create function postgres_air.great_circle_time(from_airport point, to_airport point, velocity int)
returns interval
language plpgsql
as $$
begin
  return '1 hour';
end;
$$;

create function postgres_air.schedule_arrival()
returns trigger
language plpgsql
as $$
begin
  new.scheduled_arrival := (
    select coalesce(new.actual_departure, new.scheduled_departure) +
      postgres_air.great_circle_time(ad.location, aa.location, a.velocity)
    from postgres_air.aircraft as a
    join postgres_air.airport as ad on ad.code = new.departure_airport
    join postgres_air.airport as aa on aa.code = new.arrival_airport
    where a.code = new.aircraft_code
  );

  return new;
end;
$$;

create trigger on_scheduled before insert on postgres_air.flight
for each row execute function postgres_air.schedule_arrival();

/*
create function postgres_air.flight_takeoff()
returns trigger
language plpgsql
as $$
begin
  new.scheduled_arrival := (
    select new.actual_departure + postgres_air.great_circle_time(ad.location, aa.location, a.velocity)
    from postgres_air.aircraft as a
    join postgres_air.airport as ad on ad.code = new.departure_airport
    join postgres_air.airport as aa on aa.code = new.arrival_airport
    where a.code = new.aircraft_code
  );

  return new;
end;
$$;
*/

create trigger on_takeoff before update on postgres_air.flight
for each row when (old.actual_departure is distinct from new.actual_departure)
execute function postgres_air.schedule_arrival();

create function postgres_air.delay_flight()
returns trigger
language plpgsql
as $$
begin
  update postgres_air.flight set status = 'delayed'
  where aircraft_code = new.code
    and flight_id <> new.flight_id
    and status = 'on time'
    and scheduled_departure < now() + interval '1 day';

  -- clear standbys on dependent flights (in recursive executions)
  -- delete from postgres_air.standby
  -- where flight_id = new.flight_id;

  -- rebook passengers expecting to board this aircraft
  update postgres_air.booking_leg
  set flight_id = f.flight_id
  from postgres_air.flight as f
  where postgres_air.booking_leg.flight_id = new.flight_id
    and f.departure_airport = new.departure_airport
    ;

  insert into postgres_air.credit (account_id, amount)
  select account_id, 100
  from postgres_air.passenger
  join postgres_air.booking_leg using (booking_id)
  where postgres_air.booking_leg.flight_id = new.flight_id;

  return new;
end;
$$;

create trigger on_flight_delayed after update on postgres_air.flight
for each row when (old.status <> new.status and new.status = 'delayed')
execute function postgres_air.delay_flight();

drop function if exists postgres_air.find_next_flight(char(3), char(3), timestamptz);

create or replace function postgres_air.find_next_flight(from_airport char(3), to_airport char(3), after_time timestamptz)
returns int
strict
language plpgsql
as $$
-- lower parse score!
declare newflight int;
declare one int;
declare two int;
declare three int;
declare four int;
declare five int;
declare six int;
declare seven int;
declare eight int;
declare nine int;
declare a int;
declare b int;
declare c int;
declare d int;
declare e int;
declare f int;
declare g int;
declare h int;
declare i int;
declare j int;
declare k int;
begin
  newflight := (select flight_id
  from postgres_air.flight
  where departure_airport = from_airport
    and arrival_airport = to_airport
    and scheduled_departure > after_time);

  -- more parse sandbagging
  if newflight > 1000 then
    raise exception 'nope';
  end if;

  return newflight;
end;
$$;

create function postgres_air.rebook_next_leg()
returns trigger
language plpgsql
as $$
begin
  update postgres_air.booking_leg
  set flight_id = postgres_air.find_next_flight(
    f.departure_airport,
    f.arrival_airport,
    f.scheduled_departure + interval '2 hours'
  )
  from postgres_air.booking as b
  join postgres_air.flight as f on f.flight_id = new.flight_id
  where booking_leg_id = new.booking_leg_id
    and b.booking_id = new.booking_id;

  return new;
end;
$$;

create trigger on_leg_changed after update on postgres_air.booking_leg
for each row when (old.flight_id <> new.flight_id)
execute function postgres_air.rebook_next_leg();

create function postgres_air.book_standby_check()
returns trigger
language plpgsql
as $$
declare
  already_taken int := (
    select count(*)
    from postgres_air.booking_leg
    where flight_id = new.flight_id
      and booking_id <> new.booking_id
  );
  booked_seats int := (
    select count(*)
    from postgres_air.passenger
    where booking_id = new.booking_id
  );
  aircraft_capacity int := (
    select a.capacity
    from postgres_air.flight as f
    join postgres_air.aircraft as a on a.aircraft_code = f.aircraft_code
    where f.flight_id = new.flight_id
  );
begin
  if already_taken >= aircraft_capacity then
    insert into postgres_air.standby (flight_id, passenger_id)
    select new.flight_id, p.passenger_id
    from postgres_air.passenger as p
    where p.booking_id = new.booking_id;
  end if;

  return new;
end;
$$;

create trigger on_leg_booked after insert or update on postgres_air.booking_leg
execute function postgres_air.book_standby_check();

create function postgres_air.booking_upgrade()
returns trigger
language plpgsql
as $$
declare miles_this_year int := (
  select sum(miles)
  from postgres_air.booking
  where account_id = new.account_id
);
begin
  if miles_this_year > 100000 then
    update postgres_air.account
    set tier = 'unobtainium'
    where account_id = new.account_id;
  end if;

  return new;
end;
$$;

create trigger on_booking after insert on postgres_air.booking
execute function postgres_air.booking_upgrade();

-- Views

create view postgres_air.aircraft_last_fueling as
select a.model, a.class, a.code, p.airport_code, s.name, p.supplied_at, p.ok_to_fly
from postgres_air.aircraft as a
join postgres_air.provisioning as p on p.aircraft_code = a.code
join postgres_air.supplier as s using (supplier_id)
where s.provides = 'fuel';

create view postgres_air.flight_monitor as
select f.*, a.model, a.class, g1.name as departure_gate, g2.name as arrival_gate
from postgres_air.flight as f
join postgres_air.aircraft as a on a.code = f.aircraft_code
join postgres_air.gate as g1 on g1.gate_id = f.departure_gate_id
join postgres_air.gate as g2 on g2.gate_id = f.arrival_gate_id;

create view postgres_air.arriving_today as
select *
from postgres_air.flight_monitor
where scheduled_arrival::date = now()::date;

create view postgres_air.departing_today as
select *
from postgres_air.flight_monitor
where scheduled_departure::date = now()::date;

create view postgres_air.daily_airport_activity as
select p.airport_code, count(distinct a.flight_id) as arriving, count(distinct d.flight_id) as departing
from postgres_air.airport as p
join postgres_air.arriving_today as a on a.arrival_airport = p.airport_code
join postgres_air.departing_today as d on d.departure_airport = p.airport_code
group by p.airport_code;

create view postgres_air.employee_trips as
select e.employee_id, d.name, e.first_name, e.last_name, e.hired_on, e.terminated_on, b.booking_id, jsonb_agg(
  jsonb_build_object(
    'flight', f.flight_id,
    'from', f.departure_airport,
    'to', f.arrival_airport,
    'on', f.scheduled_departure
  )
) as legs
from postgres_air.employee as e
join postgres_air.department as d using (department_id)
join postgres_air.account as a using (employee_id)
join postgres_air.booking as b using (account_id)
join postgres_air.booking_leg as l using (booking_id)
join postgres_air.flight as f using (flight_id)
group by e.employee_id, d.name, e.first_name, e.last_name, e.hired_on, e.terminated_on, b.booking_id;

create view postgres_air.employee_trips_by_destination as
select a.city, a.iso_country, max(extract (year from (legs ->> 'on')::date)) as year, count(*)
from postgres_air.employee_trips as et
join lateral jsonb_array_elements(et.legs) with ordinality as legs on true
join postgres_air.airport as a on a.airport_code = legs ->> 'to'
group by a.city, a.iso_country;

-- roles and grants

revoke all privileges on all tables in schema postgres_air from pgair_web;
revoke all privileges on all tables in schema postgres_air from pgair_agent;
revoke all privileges on all tables in schema postgres_air from pgair_employee;

drop role if exists pgair_web;
drop role if exists pgair_agent;
drop role if exists pgair_employee;

create role pgair_agent;
create role pgair_employee with role pgair_agent;
create role pgair_web;

grant select, insert, update on postgres_air.account to pgair_web;
grant select, insert, update on postgres_air.boarding_pass to pgair_web;
grant select, insert, update on postgres_air.booking to pgair_web;
grant select, insert, update on postgres_air.booking_leg to pgair_web;
grant select, insert, update on postgres_air.checked_bag to pgair_web;
grant select, insert, update on postgres_air.frequent_flyer to pgair_web;
grant select, insert, update on postgres_air.passenger to pgair_web;
grant select, insert, update on postgres_air.phone to pgair_web;

grant select on postgres_air.department to pgair_employee;
grant select, insert on postgres_air.lost_and_found to pgair_employee;
grant select, insert, update on postgres_air.provisioning to pgair_employee;

grant select on postgres_air.airport to pgair_agent;
grant select on postgres_air.gate to pgair_agent;
grant select, insert, delete on postgres_air.checked_bag to pgair_agent;
grant select, insert on postgres_air.credit to pgair_agent;
grant update on postgres_air.lost_and_found to pgair_agent;
